package br.com.neppo;

import java.util.ArrayList;

public class MathUtil {

    /**
     * Dado um conjunto de n�meros inteiros "ints" e um n�mero arbitr�rio "sum",
     * retorne true caso exista pelo menos um subconjunto de "ints" cuja soma soma dos seus elementos
     * seja igual a "sum"
     *
     * @param ints Conjunto de inteiros
     * @param sum Soma para o subconjunto
     * @return
     * @throws IllegalArgumentException caso o argumento "ints" seja null
     */
    public static boolean subsetSumChecker(int ints[], int sum) throws Exception {
        if(ints == null) {
            try {
                throw new UnsupportedOperationException();
            } catch (UnsupportedOperationException e) {
                System.out.println("testIllegalArgument: O conjunto n�o pode ser nulo!");
                return false;
            }
        }
        else{
            ArrayList<Integer> somas = new ArrayList<Integer>();
            boolean check[] = new boolean[1000];

            for(int i = 0; i < check.length; i++){
                check[i] = false;
            }

            for(int i = 0; i < ints.length; i++){//Para cada elemento Vi do vetor vet fa�a:
                if(ints[i] == sum){//Se vi = valor, retorne Sim
                    return true;
                }
                if(ints[i] < 0){//Se Vi for negativo fa�a:
                    somas.add(ints[i]);
                    int j = somas.size()-1;
                    int k = somas.size()-1;
                    while(j > 0){
                        if(somas.get(k) + somas.get(j-1) == sum){
                            System.out.println("Resposta Teste: True");
                            return true;
                        }
                        somas.add(somas.get(k) + somas.get(j-1));
                        j--;
                    }
                }
                else if((ints[i] < sum || sum == 0) && ints[i] > 0 ){//Se Vi for menor que valor fa�a:
                    check[ints[i]] = true;
                    somas.add(ints[i]);//Armazena Vi no vetor de somas
                    int j = somas.size()-1;
                    int k = somas.size()-1;
                    //Para todo valor Sj armazenado antes de Vi fa�a:
                    while(j > 0){
                        if(somas.get(k) + somas.get(j-1) == sum){//Se Vi + Sj = valor, retorne Sim
                            System.out.println("Resposta Teste: True");
                            return true;
                        }
                        //Se Vi + Sj < valor, e Vi + Sj n�o foi armazenado ainda, armazene Vi + Sj no vetor de somas.
                        else if((somas.get(k) + somas.get(j-1) < sum || sum == 0) && (check[somas.get(k) + somas.get(j-1)] == false)){
                            check[somas.get(k) + somas.get(j-1)] = true;
                            somas.add(somas.get(k) + somas.get(j-1));
                        }
                        j--;
                    }
                }
            }
            //Neste ponto , todos elementos e somas ja foram testados e nao foi encontrado nenhum subcojundo cuja a soma
            //seja igual ao valor recebido, e ent�o retorna false.
            System.out.println("Resposta Teste: False");
            return false;
        }
    }
}
