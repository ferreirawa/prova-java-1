package br.com.neppo;

import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class MathUtilTest {

    @Test
    public void testIllegalArgument() throws Exception {
        
        int ints[] = null;
        int sum = 0;

        MathUtil.subsetSumChecker(ints, sum);
    }

    @Test
    public void testSum1() throws Exception {

        int ints[] = {3, 10, 5, 7, 6, 2, 1, -15};
        int sum = 0;

        assertTrue(MathUtil.subsetSumChecker(ints, sum));
    }

    @Test
    public void testSum2() throws Exception {

        int ints[] = {3, 10, 5, 7, 6, 2, 1, -15};
        int sum = 145;

        assertFalse(MathUtil.subsetSumChecker(ints, sum));
    }
    
    @Test
    public void testSum3() throws Exception {

        int ints[] = {10, 10, 10, 15, -45};
        int sum = 45;

        assertTrue(MathUtil.subsetSumChecker(ints, sum));
    }

}
